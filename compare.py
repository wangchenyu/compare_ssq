"""compare"""


def main():
    """main"""
    right = [8, 9, 10, 13, 15, 28]
    fp = open('number.txt', 'r', encoding='utf8')
    content = fp.readlines()
    nums = [[x[:2], x[3:5], x[6:8], x[9:11], x[12:14], x[15:17]]
            for x in content]
    result_3, result_4, result_5, result_6 = [], [], [], []
    for one in nums:
        count = 0
        for everyone in one:
            if int(everyone) in right:
                count += 1
        if count == 3:
            result_3.append(one[:])
        if count == 4:
            result_4.append(one[:])
        if count == 5:
            result_5.append(one[:])
        if count == 6:
            result_6.append(one[:])
    print(">>> right 3:", result_3)
    print(">>> right 4:", result_4)
    print(">>> right 5:", result_5)
    print(">>> right 6:", result_6)


if __name__ == "__main__":
    main()
